import os
from datetime import datetime

import numpy as np
import pandas as pd
from jinja2 import Environment, FileSystemLoader
import bokeh
from bokeh.plotting import figure, ColumnDataSource
from bokeh.models.ranges import Range1d
from bokeh.models import BoxSelectTool
from bokeh.embed import components

import js_helpers

def create_data(N):
    df = pd.DataFrame({
        "Gaussian":np.random.randn(N),
        "Pareto":np.random.pareto(5, size=N),
        "Sine":2 * np.sin(np.linspace(0,2 * np.pi, N))
    })

    df["Gaussian"] = df["Gaussian"].sort_values().reset_index(drop=True)
    df["Pareto"] = df["Pareto"].sort_values().reset_index(drop=True)
    
    return df

def make_plot(df, kind="line"):

    if kind.lower() not in ["line", "hist"]:
        raise Exception(kind, "plot type not supported")

    cmap = {
        "Gaussian":"lightcoral",
        "Pareto":"dodgerblue",
        "Sine":"darkcyan"
    }

    tooltips = [
        ("(x,y)", "($x, $y)"),
        ("x", "@index")
    ] + [(col, "@"+col) for col in df.columns]


    plot = figure(sizing_mode="stretch_width", height=400, tooltips=tooltips)

    if kind.lower() == "line":
        source = ColumnDataSource(df.reset_index())
        for column in df:
            plot.line(x="index", y=column, source=source, line_width=2,
            line_color=cmap[column], legend_label=column, muted_alpha=0.1)
        plot.legend.click_policy = "mute"
   
    elif kind.lower() == "hist":
        bins = np.linspace(df.min().min(), df.max().max(), 50)

        #create histogram dataframe
        hist = {}
        for column in df:
            counts, _ = np.histogram(df[column], bins=bins)
            hist[column] = 100 * counts / len(df)
        hist["bins"] = bins[:-1] + 0.5 * (bins[1:] - bins[:-1])
        hist = pd.DataFrame(hist)
        source = ColumnDataSource(hist)
        for column in df:
            plot.vbar(x="bins", top=column, bottom=0, width=bins[1] - bins[0],
            source=source, line_width=2, line_color="darkslategray",
            fill_color=cmap[column], muted_alpha=0.1,
            legend_label=column)
        
        plot.yaxis.axis_label = "Value (%)"
        t = BoxSelectTool()
        plot.add_tools(t)
        plot.toolbar.active_drag = t

        source.selected.js_on_change("indices", js_helpers.hist(source))
    
    plot.legend.click_policy = "mute"
    return plot

if __name__ == "__main__":

    df = create_data(10000)
    classes = "table table-striped table-hover table-bordered"
    formatters = {a:lambda x: f"{x:.1f}" for a in df.columns}
    table_html = df.describe().to_html(classes=classes, formatters=formatters)

    plot_1 = make_plot(df)
    plot_1.legend.location = "top_left"
    plot_2 = make_plot(df,"hist")

    plot_1.y_range = Range1d(df.min().min()*0.99, df.max().max()*1.01)
    plot_2.x_range = plot_1.y_range

    env = Environment(loader=FileSystemLoader("templates"))
    
    with open("public/data.html","w+") as fd:
        template = env.get_template("data.html")
        script, divs = components([plot_1, plot_2])
        fd.write(
            template.render(
                table_data=table_html,
                bokeh_version=bokeh.__version__,
                plot_script=script,
                plot_div_1=divs[0], 
                plot_div_2=divs[1],
                datestr=datetime.now().strftime("%A %d %B at %I:%M%p"),
                n_rows=df.shape[0],
                n_cols=df.shape[1]
                )
            )

    with open("public/bonus.html","w+") as fd:
        template = env.get_template("bonus.html")
        fd.write(
            template.render(id=os.getenv("BONUS_URL"))
            )

