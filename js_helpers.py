from bokeh.models import CustomJS

def hist(source):
    return CustomJS(args={"source":source}, code = """
    const inds = source.selected.indices;
    const d = source.data;

    if (inds.length == 0)
    {
        document.getElementById("ave_hist_gauss").innerHTML = "-";
        document.getElementById("ave_hist_pareto").innerHTML = "-";
        document.getElementById("ave_hist_sine").innerHTML = "-";
        return;
    }

    var ave_g = 0.0, ave_p = 0.0, ave_s = 0.0;
    for (var i = 0; i < inds.length; i++) {
        ave_g += d['Gaussian'][inds[i]]
        ave_p += d['Pareto'][inds[i]]
        ave_s += d['Sine'][inds[i]]
    }

    document.getElementById("hist_ave_gauss").innerHTML = `${ave_g.toFixed(1).toString()}%`;
    document.getElementById("hist_ave_pareto").innerHTML = `${ave_p.toFixed(1).toString()}%`;
    document.getElementById("hist_ave_sine").innerHTML = `${ave_s.toFixed(1).toString()}%`;
    """)